mkdir bundle || { echo "Aborting..." ; exit 1; }

# documentation
cp LICENCE bundle/
cp README.md bundle/

# code
cp *.py bundle/
cp -r templates bundle/

# transparency of data generation
cp build-artifact.sh bundle/

zip -r processnet.zip bundle
