import unittest

class NetTest(unittest.TestCase):
    def setUp(self):
        import net as pn  # type: ignore
        self.places = pn.PlaceSet({"p1", "p2", "p3", "p4", "p5", "p6", "p7",
                                   "p8"})

        self.transitions = (
                    pn.Transition(
                        pn.PlaceSet({"p1", "p3"}),
                        pn.PlaceSet({"p1", "p7"})),
                    pn.Transition(
                        pn.PlaceSet({"p2", "p4"}),
                        pn.PlaceSet({"p4", "p8"})),
                    pn.Transition(
                        pn.PlaceSet({"p3", "p1"}),
                        pn.PlaceSet({"p2", "p7"})),
                    pn.Transition(
                        pn.PlaceSet({"p2", "p3"}),
                        pn.PlaceSet({"p2", "p7"})))

        self.net = pn.Processnet(self.places, self.transitions)

    def test_indexing(self):
        for p in self.places:
            self.assertEqual(p, self.net.get_place(self.net.get_index(p)))
        self.assertEqual(
                self.net.indices,
                frozenset({self.net.get_index(p) for p in self.places}))
        self.assertEqual(
                self.places,
                frozenset({self.net.get_place(i) for i in self.net.indices}))


if __name__ == '__main__':
    unittest.main()
