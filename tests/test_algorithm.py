import unittest
import algorithm as alg
import net as pn  # type: ignore

class NetTest(unittest.TestCase):
    def setUp(self):
        self.places = pn.PlaceSet({"p1", "p2", "p3"})

        self.transitions = (
                    pn.Transition(
                        pn.PlaceSet({"p1", "p2"}),
                        pn.PlaceSet({"p3", "p2"})),)

        self.net = pn.Processnet(self.places, self.transitions)


    def test_traps(self):
        marking = pn.Marking(self.net, pn.PlaceSet({"p1"}))
        bad = pn.Marking(self.net, pn.PlaceSet({"p3"}))
        self.assertIn(alg.find_trap(marking, bad),
                      {frozenset({"p1", "p2"})})

    def test_flows(self):
        marking = pn.Marking(self.net, pn.PlaceSet({"p1", "p2"}))
        bad = pn.Marking(self.net, pn.PlaceSet({"p3"}))
        self.assertIn(alg.find_flow(marking, bad),
                      {frozenset({"p2"})})

    def test_siphons(self):
        marking = pn.Marking(self.net, pn.PlaceSet({"p1"}))
        bad = pn.Marking(self.net, pn.PlaceSet({"p3", "p2"}))
        self.assertIn(alg.find_siphon(marking, bad),
                      {frozenset({"p2"}),
                       frozenset({"p2", "p3"})})

    def test_unreach(self):
        marking = pn.Marking(self.net, pn.PlaceSet({"p1"}))
        bad = [pn.Marking(self.net, pn.PlaceSet({"p3"})),
                pn.Marking(self.net, pn.PlaceSet({"p2", "p3"}))]
        result, ce, invariants_syn = alg.check_reach(marking, bad)
        self.assertFalse(result)
        self.assertIsNone(ce)

    def test_reach(self):
        marking = pn.Marking(self.net, pn.PlaceSet({"p1", "p2"}))
        reachable = [pn.Marking(self.net, pn.PlaceSet({"p3"}))]
        result, ce, invariants_syn = alg.check_reach(marking, reachable)
        self.assertTrue(result)
        self.assertIsNotNone(ce)
        self.assertEqual(marking, ce)

    @staticmethod
    def dijkstra_no_turn_net(n):
        initial = {
                f"b/{i}/false"
                for i in range(n)
                } | {
                f"state/{i}/initial"
                for i in range(n)
                }
        places = set()
        transitions = []
        for i in range(n):
            places |= {
                    f"b/{i}/false",
                    f"b/{i}/true",
                    f"state/{i}/initial",
                    f"state/{i}/loop",
                    f"state/{i}/break",
                    f"state/{i}/crit",
                    f"state/{i}/done"}
            places |= {
                    f"state/{i}/loop/{j}"
                    for j in range(n+1)}
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/false"}),
                        pn.PlaceSet({f"state/{i}/loop", f"b/{i}/true"})
                        ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/true"}),
                        pn.PlaceSet({f"state/{i}/loop", f"b/{i}/true"})
                        )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop"}),
                        pn.PlaceSet({f"state/{i}/loop/0"})
                        ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop/{i}"}),
                        pn.PlaceSet({f"state/{i}/loop/{i+1}"})
                        )
                    ] + [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop/{j}", f"b/{j}/false"}),
                        pn.PlaceSet({f"state/{i}/loop/{j+1}", f"b/{j}/false"})
                        )
                    for j in range(n)
                    ] + [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop/{j}", f"b/{j}/true"}),
                        pn.PlaceSet({f"state/{i}/break", f"b/{j}/true"})
                        )
                    for j in range(n)
                    ] + [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop/{n}"}),
                        pn.PlaceSet({f"state/{i}/crit"})
                        )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/break", f"b/{i}/false"}),
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/false"})
                        ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/break", f"b/{i}/true"}),
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/false"})
                        )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/crit"}),
                        pn.PlaceSet({f"state/{i}/done"})
                        )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/done", f"b/{i}/false"}),
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/false"})
                        ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/done", f"b/{i}/true"}),
                        pn.PlaceSet({f"state/{i}/initial", f"b/{i}/false"})
                        )
                    ]
        return pn.Marking(
                pn.Processnet(
                    frozenset(places),
                    tuple(transitions),
                ), initial)

    @staticmethod
    def dijkstra_net(n):
        initial = {
                    f"c/{i}/1"
                    for i in range(1, n+1)
                } | {
                    f"b/{i}/1"
                    for i in range(1, n+1)
                } | {
                    f"state/{i}/s1"
                    for i in range(1, n+1)
                } | {
                    "turn/0"
                }
        places = {
            f"turn/{j}"
            for j in range(0, n+1)}
        transitions = []
        for i in range(1, n+1):
            places |= {
                    f"state/{i}/s1",
                    f"state/{i}/s2",
                    f"state/{i}/s3",
                    f"state/{i}/s4",
                    f"state/{i}/s5",
                    f"state/{i}/s6",
                    f"state/{i}/s7",
                    f"state/{i}/s8",
                    f"state/{i}/s10",
                    f"state/{i}/s11",
                    f"state/{i}/s12",
                    f"state/{i}/s13",
                    f"state/{i}/s14",
                    f"state/{i}/s15",
                    f"c/{i}/0",
                    f"c/{i}/1",
                    f"b/{i}/0",
                    f"b/{i}/1",
            }
            places |= {
                    f"state/{i}/s9/counter/{j}"
                    for j in range(1, n+2)}
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s1", f"b/{i}/1"}),
                        pn.PlaceSet({f"state/{i}/s2", f"b/{i}/0"})
                    ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/s1", f"b/{i}/0"}),
                        pn.PlaceSet({f"state/{i}/s2", f"b/{i}/0"}))]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s2", f"turn/{j}"}),
                        pn.PlaceSet({f"state/{i}/s3", f"turn/{j}"})
                    ) for j in range(0, n+1) if j != i]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s2", f"turn/{i}"}),
                        pn.PlaceSet({f"state/{i}/s7", f"turn/{i}"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s3", f"c/{i}/1"}),
                        pn.PlaceSet({f"state/{i}/s4", f"c/{i}/1"})
                    ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/s3", f"c/{i}/0"}),
                        pn.PlaceSet({f"state/{i}/s4", f"c/{i}/1"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s4", f"turn/0"}),
                        pn.PlaceSet({f"state/{i}/s5", f"turn/0"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s4", f"turn/{j}", f"b/{j}/1"}),
                        pn.PlaceSet({f"state/{i}/s5", f"turn/{j}", f"b/{j}/1"})
                    ) for j in range(1, n+1)]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s5", f"turn/{j}"}),
                        pn.PlaceSet({f"state/{i}/s6", f"turn/{i}"})
                    ) for j in range(0, n+1)]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s6"}),
                        pn.PlaceSet({f"state/{i}/s2"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s7", f"c/{i}/0"}),
                        pn.PlaceSet({f"state/{i}/s8", f"c/{i}/0"})
                    ), pn.Transition(
                        pn.PlaceSet({f"state/{i}/s7", f"c/{i}/1"}),
                        pn.PlaceSet({f"state/{i}/s8", f"c/{i}/0"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s8"}),
                        pn.PlaceSet({f"state/{i}/s9/counter/1"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s9/counter/{i}"}),
                        pn.PlaceSet({f"state/{i}/s9/counter/{i+1}"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s9/counter/{j}", f"c/{j}/1"}),
                        pn.PlaceSet({f"state/{i}/s9/counter/{j+1}", f"c/{j}/1"})
                    ) for j in range(1, n+1) if j != i]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s9/counter/{j}", f"c/{j}/0"}),
                        pn.PlaceSet({f"state/{i}/s10", f"c/{j}/0"})
                    ) for j in range(1, n+1) if j != i]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s10"}),
                        pn.PlaceSet({f"state/{i}/s2"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s9/counter/{n+1}"}),
                        pn.PlaceSet({f"state/{i}/s11"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s11"}),
                        pn.PlaceSet({f"state/{i}/s12"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s12", f"turn/{j}"}),
                        pn.PlaceSet({f"state/{i}/s13", f"turn/0"})
                    ) for j in range(0, n+1)]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s13", f"c/{i}/0"}),
                        pn.PlaceSet({f"state/{i}/s14", f"c/{i}/1"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s13", f"c/{i}/1"}),
                        pn.PlaceSet({f"state/{i}/s14", f"c/{i}/1"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s14", f"b/{i}/0"}),
                        pn.PlaceSet({f"state/{i}/s15", f"b/{i}/1"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s14", f"b/{i}/1"}),
                        pn.PlaceSet({f"state/{i}/s15", f"b/{i}/1"})
                    )]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/s15"}),
                        pn.PlaceSet({f"state/{i}/s1"})
                    )]
        return pn.Marking(
                pn.Processnet(
                    frozenset(places),
                    tuple(transitions)
                ), initial)

    @staticmethod
    def eisenberg_mcguire_net(n):
        places = (
            (initial := {
                        f"control/{i}/neutral"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/initial"
                        for i in range(1, n+1)
                    } | {
                        "k/1"
                    }) | {
                        f"k/{j}"
                        for j in range(1, n+1)
                    } | {
                        f"control/{i}/firstLevel"
                        for i in range(1, n+1)
                    } | {
                        f"control/{i}/secondLevel"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/{j}"
                        for i in range(1, n+1)
                        for j in range(1, n+2)
                    } | {
                        f"state/{i}/loop2/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop2/{j}"
                        for i in range(1, n+1)
                        for j in range(1, n+2)
                    } | {
                        f"state/{i}/passFirstCheck"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/{j}"
                        for i in range(1, n+1)
                        for j in range(1, n+2)
                    } | {
                        f"state/{i}/checkK"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/setK"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop4/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop4/{j}"
                        for i in range(1, n+1)
                        for j in range(1, n+2)
                    } | {
                        f"state/{i}/loop5/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop5/{j}"
                        for i in range(1, n+1)
                        for j in range(1, n+2)
                    } | {
                        f"state/{i}/CS"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/endCritical"
                        for i in range(1, n+1)
                    })
        transitions = []
        for i in range(1, n+1):
            # control[i] := 1
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    ]
            # for j := k step 1 until N
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/init", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"k/{j}"}),
                    )
                    for j in range(1, n+1)
                    ]
            # if j = i then goto L2
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{i}"}),
                        pn.PlaceSet({f"state/{i}/passFirstCheck"}),
                    ),
                    ]
            # if control[j] != neutral goto L1
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            # step 1
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j+1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            # 1 step 1 until k
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{n+1}"}),
                        pn.PlaceSet({f"state/{i}/loop2/1"})
                    ),
                    ]
            # if j = i then goto L2
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop2/{i}"}),
                        pn.PlaceSet({f"state/{i}/passFirstCheck"}),
                    ),
                    ]
            # if control[j] != neutral goto L1
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop2/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop2/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            # step 1
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop2/{j}", f"control/{j}/neutral", f"k/{k}"}),
                        pn.PlaceSet({f"state/{i}/loop2/{j+1}", f"control/{j}/neutral", f"k/{k}"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if j != i and k != j
                    ]
            # until k
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop2/{j}", f"control/{j}/neutral", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{j}/neutral", f"k/{j}"})
                    )
                    for j in range(1, n)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/init"}),
                        pn.PlaceSet({f"state/{i}/loop3/1"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j+1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{i}"}),
                        pn.PlaceSet({f"state/{i}/loop3/{i+1}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j+1}", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{n+1}"}),
                        pn.PlaceSet({f"state/{i}/checkK"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/checkK", f"k/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/setK", f"k/{j}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/checkK", f"k/{i}"}),
                        pn.PlaceSet({f"state/{i}/setK", f"k/{i}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/checkK", f"k/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"k/{j}", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/checkK", f"k/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"k/{j}", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/setK", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/CS", f"k/{i}"})
                    )
                    for j in range(1, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS"}),
                        pn.PlaceSet({f"state/{i}/loop4/init"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/init", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/loop4/{j}", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/{j}", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/loop4/{j+1}", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/{j}", f"k/{k}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop4/{j+1}", f"k/{k}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/{j}", f"control/{j}/firstLevel", f"k/{k}"}),
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{j}/firstLevel", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/{j}", f"control/{j}/secondLevel", f"k/{k}"}),
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{j}/secondLevel", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop4/{n+1}"}),
                        pn.PlaceSet({f"state/{i}/loop5/1"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop5/{j}", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/endCritical", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop5/{j}", f"k/{k}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop5/{j+1}", f"k/{k}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop5/{j}", f"control/{j}/firstLevel", f"k/{k}"}),
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{j}/firstLevel", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop5/{j}", f"control/{j}/secondLevel", f"k/{k}"}),
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{j}/secondLevel", f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    for k in range(1, n+1)
                    if k != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/endCritical", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"})
                    ),
                    ]

        return pn.Marking(
                pn.Processnet(
                    frozenset(places),
                    tuple(transitions)
                ), initial)

    @staticmethod
    def knuth_net(n):
        places = (
            (initial := {
                        f"control/{i}/neutral"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/initial"
                        for i in range(1, n+1)
                    } | {
                        "k/0"
                    }) | {
                        f"k/{j}"
                        for j in range(1, n+1)
                    } | {
                        f"control/{i}/firstLevel"
                        for i in range(1, n+1)
                    } | {
                        f"control/{i}/secondLevel"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/{j}"
                        for i in range(1, n+1)
                        for j in range(0, n+1)
                    } | {
                        f"state/{i}/passFirstCheck"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/{j}"
                        for i in range(1, n+1)
                        for j in range(0, n+1)
                    } | {
                        f"state/{i}/setK"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/CS"
                        for i in range(1, n+1)
                    })
        transitions = []
        for i in range(1, n+1):
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/init", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"k/{j}"}),
                    )
                    for j in range(0, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{i}"}),
                        pn.PlaceSet({f"state/{i}/passFirstCheck"}),
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j-1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/0"}),
                        pn.PlaceSet({f"state/{i}/loop1/{n}"})
                    )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/init"}),
                        pn.PlaceSet({f"state/{i}/loop3/{n}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j-1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{i}"}),
                        pn.PlaceSet({f"state/{i}/loop3/{i-1}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j-1}", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/0"}),
                        pn.PlaceSet({f"state/{i}/setK"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/setK", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/CS", f"k/{i}"})
                    )
                    for j in range(0, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/initial", (f"k/{j-1}"
                                                            if j > 0
                                                            else f"k/{n}")})
                    )
                    for j in range(0, n+1)
                    ]

        return pn.Marking(
                pn.Processnet(
                    frozenset(places),
                    tuple(transitions)
                ), initial)

    @staticmethod
    def deBruijn_net(n):
        places = (
            (initial := {
                        f"control/{i}/neutral"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/initial"
                        for i in range(1, n+1)
                    } | {
                        "k/1"
                    }) | {
                        f"k/{j}"
                        for j in range(1, n+1)
                    } | {
                        f"control/{i}/firstLevel"
                        for i in range(1, n+1)
                    } | {
                        f"control/{i}/secondLevel"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop1/{j}"
                        for i in range(1, n+1)
                        for j in range(0, n+1)
                    } | {
                        f"state/{i}/passFirstCheck"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/init"
                        for i in range(1, n+1)
                    } | {
                        f"state/{i}/loop3/{j}"
                        for i in range(1, n+1)
                        for j in range(0, n+1)
                    } | {
                        f"state/{i}/CS"
                        for i in range(1, n+1)
                    })
        transitions = []
        for i in range(1, n+1):
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/initial", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{i}/firstLevel"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/init", f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"k/{j}"}),
                    )
                    for j in range(1, n+1)
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{i}"}),
                        pn.PlaceSet({f"state/{i}/passFirstCheck"}),
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop1/{j-1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop1/init", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop1/0"}),
                        pn.PlaceSet({f"state/{i}/loop1/{n}"})
                    )
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/passFirstCheck", f"control/{i}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/init", f"control/{i}/secondLevel"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/init"}),
                        pn.PlaceSet({f"state/{i}/loop3/{n}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/neutral"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j-1}", f"control/{j}/neutral"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{i}"}),
                        pn.PlaceSet({f"state/{i}/loop3/{i-1}"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/firstLevel"}),
                        pn.PlaceSet({f"state/{i}/loop3/{j-1}", f"control/{j}/firstLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/{j}", f"control/{j}/secondLevel"}),
                        pn.PlaceSet({f"state/{i}/initial", f"control/{j}/secondLevel"})
                    )
                    for j in range(1, n+1)
                    if i != j
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/loop3/0"}),
                        pn.PlaceSet({f"state/{i}/CS"})
                    ),
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS",
                                     f"control/{j}/neutral",
                                     f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/initial",
                                     f"control/{j}/neutral",
                                     (f"k/{j-1}" if j > 1 else f"k/{n}")})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS",
                                     f"control/{j}/firstLevel",
                                     f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/initial",
                                     f"control/{j}/firstLevel",
                                     f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS",
                                     f"control/{j}/secondLevel",
                                     f"k/{j}"}),
                        pn.PlaceSet({f"state/{i}/initial",
                                     f"control/{j}/secondLevel",
                                     f"k/{j}"})
                    )
                    for j in range(1, n+1)
                    if j != i
                    ]
            transitions += [
                    pn.Transition(
                        pn.PlaceSet({f"state/{i}/CS",
                                     f"k/{i}"}),
                        pn.PlaceSet({f"state/{i}/initial",
                                     (f"k/{i-1}" if i > 1 else f"k/{n}")})
                    ),
                    ]

        return pn.Marking(
                pn.Processnet(
                    frozenset(places),
                    tuple(transitions)
                ), initial)

    def test_dijkstra_with_turn(self):
        for n in range(2, 5):
            with self.subTest(msg=(f"Testing Dijkstra's mutex algorithm "
                                   f"with turn variable with N={n}")):
                marking = self.dijkstra_net(n)
                bad_covers = [pn.Marking(
                    marking.net,
                    pn.PlaceSet({f"state/{i}/s11", f"state/{j}/s11"}))
                    for j in range(1, n+1)
                    for i in range(1, n+1) if i != j]
                self.assertEqual(len(bad_covers), n*(n-1))

                result, ce, invariants = alg.check_reach(marking, bad_covers)
                self.assertFalse(result)
                self.assertIsNone(ce)

    def test_dijkstra_without_turn(self):
        for n in range(3, 4):
            with self.subTest(msg=(f"Testing Dijkstra's mutex algorithm "
                                   f"without turn variable with N={n}")):
                marking = self.dijkstra_no_turn_net(n)
                bad_covers = [pn.Marking(
                    marking.net,
                    pn.PlaceSet({f"state/{i}/crit", f"state/{j}/crit"}))
                    for j in range(n)
                    for i in range(n) if i != j]
                self.assertEqual(len(bad_covers), n*(n-1))

                result, ce, invariants = alg.check_reach(marking, bad_covers)
                self.assertFalse(result)
                self.assertIsNone(ce)

    def test_knuth(self):
        for n in range(2, 5):
            with self.subTest(msg=(f"Testing Knuth's mutex algorithm "
                                   f"with N={n}")):
                marking = self.knuth_net(n)
                bad_covers = [pn.Marking(
                    marking.net,
                    pn.PlaceSet({f"state/{i}/CS", f"state/{j}/CS"}))
                    for j in range(1, n+1)
                    for i in range(1, n+1) if i != j]
                self.assertEqual(len(bad_covers), n*(n-1))

                result, ce, invariants = alg.check_reach(marking, bad_covers)
                self.assertFalse(result)
                self.assertIsNone(ce)

    def test_deBruijn(self):
        for n in range(2, 5):
            with self.subTest(msg=(f"Testing de Bruijn's mutex algorithm "
                                   f"with N={n}")):
                marking = self.deBruijn_net(n)
                bad_covers = [pn.Marking(
                    marking.net,
                    pn.PlaceSet({f"state/{i}/CS", f"state/{j}/CS"}))
                    for j in range(1, n+1)
                    for i in range(1, n+1) if i != j]
                self.assertEqual(len(bad_covers), n*(n-1))

                result, ce, invariants = alg.check_reach(marking, bad_covers)
                self.assertFalse(result)
                self.assertIsNone(ce)

    def test_eisenberg_mcguire(self):
        for n in range(2, 5):
            with self.subTest(msg=(f"Testing Eisenberg and McGuire's mutex "
                                   f"algorithm with N={n}")):
                marking = self.eisenberg_mcguire_net(n)
                bad_covers = [pn.Marking(
                    marking.net,
                    pn.PlaceSet({f"state/{i}/CS", f"state/{j}/CS"}))
                    for j in range(1, n+1)
                    for i in range(1, n+1) if i != j]
                self.assertEqual(len(bad_covers), n*(n-1))

                result, ce, invariants = alg.check_reach(marking, bad_covers)

                self.assertFalse(result)
                self.assertIsNone(ce)

    @unittest.skip("This works and is left here for documentation; but I do "
                   "not know how to properly test that `asyncio.to_thread` "
                   "can be applied to `alg.check_reach`.")
    def test_asyncio_thread(self):
        import asyncio

        async def tick():
            t = 0
            while True:
                print(t, flush=True)
                await asyncio.sleep(1)
                t += 1

        n = 4
        marking = self.eisenberg_mcguire_net(n)
        bad_covers = [pn.Marking(
            marking.net,
            pn.PlaceSet({f"state/{i}/CS", f"state/{j}/CS"}))
            for j in range(1, n+1)
            for i in range(1, n+1) if i != j]


        async def test_async_processnet():
            ticking_task = asyncio.create_task(tick())
            result, ce, invariants = await asyncio.to_thread(alg.check_reach, marking, bad_covers)
            self.assertFalse(result)
            self.assertIsNone(ce)
            ticking_task.cancel()
        asyncio.run(test_async_processnet())

if __name__ == '__main__':
    unittest.main()
