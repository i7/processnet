import typing
import dataclasses


Place = typing.Hashable
PlaceSet = frozenset


class Transition(typing.NamedTuple):
    preset: PlaceSet
    postset: PlaceSet


@dataclasses.dataclass(frozen=True)
class Processnet:
    places: PlaceSet
    transitions: typing.Tuple[Transition, ...]

    def __post_init__(self):
        # consistency requirements
        for pre, post in self.transitions:
            if not (pre <= self.places and post <= self.places):
                raise ValueError(
                        "Cannot instantiate net with transition "
                        "({pre}, {post}): unknown places "
                        "{unknown}".format(
                            pre="{" + ", ".join(str(p) for p in pre) + "}",
                            post="{" + ", ".join(str(p) for p in post) + "}",
                            unknown= (pre - self.places) | (post - self.places)
                        ))
        # construct lookup tables for places to indices and back
        place_index_map = {}
        index_place_map = {}
        for index, place in enumerate(self.places):
            place_index_map[place] = index
            index_place_map[index] = place
        object.__setattr__(self, "place_index_map", place_index_map)
        object.__setattr__(self, "index_place_map", index_place_map)

    def get_index(self, place: Place) -> int:
        return self.place_index_map[place]  # type: ignore

    def get_place(self, index: int) -> Place:
        return self.index_place_map[index]  # type: ignore

    @property
    def indices(self) -> typing.FrozenSet[int]:
        return frozenset(self.index_place_map)  # type: ignore


class Marking(typing.NamedTuple):
    net: Processnet
    marked_places: PlaceSet
