from processnet import net as pn  # type: ignore

import typing
import jinja2  # type: ignore
import clingo  # type: ignore
import dataclasses
import multiprocessing
import multiprocessing.connection
import dataclasses


Pipe = typing.Tuple[multiprocessing.connection.Connection,
                    multiprocessing.connection.Connection]


ClingoVariable = typing.NewType("ClingoVariable", str)
Model = typing.Mapping[ClingoVariable, typing.Tuple[int, ...]]

env = jinja2.Environment(
        undefined=jinja2.StrictUndefined,
        loader=jinja2.PackageLoader('processnet'))

def place_term(place: pn.Place,
               net: pn.Processnet,
               name: str = "place") -> ClingoVariable:
    return ClingoVariable(f"{name}({net.get_index(place)})")


env.filters["place_term"] = place_term


def clingo_solve(formula: str) -> typing.Optional[Model]:
    def get_model(model: "clingo.Model") -> Model:
        res: typing.Dict[str, typing.List[int]] = {}
        for symb in model.symbols(atoms=True, terms=True, shown=False):
            if symb.type is clingo.SymbolType.Function:
                name = symb.name
                if name not in res:
                    res[name] = []
                if not (len(symb.arguments) == 1
                        and symb.arguments[0].type is clingo.SymbolType.Number):
                    raise ValueError(f"Unexpected symbol: {symb}")
                res[name].append(symb.arguments[0].number)
        return {ClingoVariable(n): tuple(sorted(res[n])) for n in res}

    ctl = clingo.Control(message_limit=0)
    ctl.configuration.solve.models = 1
    ctl.add("base", [], formula)
    ctl.ground([("base", [])])
    with ctl.solve(yield_=True) as handle:
        try:
            res = next(handle)
            return get_model(res)
        except StopIteration:
            return None


def get_place_set(net: pn.Processnet,
                  model: typing.Optional[Model],
                  name: ClingoVariable) -> pn.PlaceSet:
    if model:
        return frozenset({net.get_place(i) for i in model.get(name, [])})
    else:
        return frozenset()


class Invariants(typing.NamedTuple):
    traps: typing.Collection[pn.PlaceSet]
    flows: typing.Collection[pn.PlaceSet]
    siphons: typing.Collection[pn.PlaceSet]


def check_reach(marking: pn.Marking,
                bad_covers: typing.Collection[pn.Marking]
                ) -> typing.Tuple[bool,
                                  typing.Optional[pn.Marking],
                                  Invariants]:
    traps: typing.Set[pn.PlaceSet] = set()
    flows: typing.Set[pn.PlaceSet] = set()
    siphons: typing.Set[pn.PlaceSet] = set()

    for cover in bad_covers:
        for t in marking.net.transitions:
            while ce := get_counter_example(
                    marking.net,
                    t,
                    cover,
                    Invariants(traps, flows, siphons)):
                if trap := find_trap(marking, ce):
                    traps.add(trap)
                    continue
                elif siphon := find_siphon(marking, ce):
                    siphons.add(siphon)
                    continue
                elif flow := find_flow(marking, ce):
                    flows.add(flow)
                    continue
                else:
                    invariants = Invariants(traps, flows, siphons)
                    return True, ce, invariants
    invariants = Invariants(traps, flows, siphons)
    return False, None, invariants


def get_counter_example_formula(
        net: pn.Processnet,
        transition: pn.Transition,
        cover: pn.Marking,
        invariants: Invariants) -> str:
    formula = env.get_template("reaches.clingo").render(
            net=net,
            t=transition,
            traps=invariants.traps,
            flows=invariants.flows,
            siphons=invariants.siphons,
            bad_places=cover.marked_places)
    return formula


def get_counter_example(
        net: pn.Processnet,
        transition: pn.Transition,
        cover: pn.Marking,
        invariants: Invariants) -> typing.Optional[pn.Marking]:
    formula = get_counter_example_formula(
            net,
            transition,
            cover,
            invariants)
    if marked_places := get_place_set(net,
                                      clingo_solve(formula),
                                      ClingoVariable("before")):
        return pn.Marking(net, marked_places)
    else:
        return None


def find_trap_formula(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]) -> str:
    net, initial = marking
    if invalidate:
        if not invalidate.net == net:
            raise ValueError(f"Cannot invalidate marking in different net.")
        invalidated_places = invalidate.marked_places
    template = env.get_template('trap_constraint.clingo')
    formula = template.render(net=net,
                              initial=initial,
                              invalidate=invalidated_places)
    return formula


def find_trap(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]
        ) -> pn.PlaceSet:
    formula = find_trap_formula(marking, invalidate)
    return get_place_set(marking.net, clingo_solve(formula),
                         ClingoVariable("place"))


def find_flow_formula(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]) -> str:
    net, initial = marking
    if invalidate:
        if not invalidate.net == net:
            raise ValueError(f"Cannot invalidate marking in different net.")
        invalidated_places = invalidate.marked_places
    template = env.get_template('flow_constraint.clingo')
    formula = template.render(net=net,
                              initial=initial,
                              invalidate=invalidated_places)
    return formula


def find_flow(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]
        ) -> pn.PlaceSet:
    formula = find_flow_formula(marking, invalidate)
    return get_place_set(marking.net, clingo_solve(formula),
                         ClingoVariable("place"))


def find_siphon_formula(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]) -> str:
    net, initial = marking
    if invalidate:
        if not invalidate.net == net:
            raise ValueError(f"Cannot invalidate marking in different net.")
        invalidated_places = invalidate.marked_places
    template = env.get_template('siphon_constraint.clingo')
    formula = template.render(net=net,
                              initial=initial,
                              invalidate=invalidated_places)
    return formula


def find_siphon(
        marking: pn.Marking,
        invalidate: typing.Optional[pn.Marking]
        ) -> pn.PlaceSet:
    formula = find_siphon_formula(marking, invalidate)
    return get_place_set(marking.net, clingo_solve(formula),
                         ClingoVariable("place"))
